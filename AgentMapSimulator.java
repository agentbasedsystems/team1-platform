package org.xploration.team1.platform;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.TreeMap;
import java.util.Properties;

import org.xploration.ontology.*;

import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.WakerBehaviour;

public class AgentMapSimulator extends Agent{
	
	private static final long serialVersionUID = 1L;
	private int mapHeight= 0;
    private int mapWidth= 0;
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    Object[] argsArray = new Object[1];
    
    private String mapFileName = "map.txt";
    private String configurationFileName = "xploration.properties";
    
    private int missionLength = 0;
    private int radioRange = 0;
    private int registrationWindow = 0;
    private int analysisTime = 0;
    private int movementTime = 0;
	
	protected void setup()
	{
		System.out.println(getLocalName()+": Entered the system.");

		// Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        try {
			// Creates its own description
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setName(this.getName());
			sd.setType(PlatformConstants.MAP_SIMULATOR_SERVICE_NAME);
			dfd.addServices(sd);
			// Registers its description in the DF
			DFService.register(this, dfd);
			System.out.println(getLocalName()+": Registered in the DF.");
			dfd = null;
			sd = null;
			
		} catch (FIPAException e) {
			e.printStackTrace();
		}
        
        // Read configuration file
        readConfigurationData();
        
        // Read Map file and create map        
        MineralMap mineralsMap = new MineralMap(createMap(readFile(mapFileName)), mapHeight, mapWidth);
        argsArray[0] = mineralsMap;
        
        // Create Terrain and pass the map
        instantiateTerrainSimulator();
        
        addBehaviour(missionLengthCountdown());
	}

	private void readConfigurationData() {
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream(configurationFileName);
			// load a properties file
			prop.load(input);
			
		    missionLength = Integer.valueOf(prop.getProperty("MISSION_LENGTH"));
		    radioRange = Integer.valueOf(prop.getProperty("RADIO_RANGE"));
		    registrationWindow = Integer.valueOf(prop.getProperty("REGISTRATION_WINDOW"));
		    analysisTime = Integer.valueOf(prop.getProperty("ANALYSIS_TIME"));
		    movementTime = Integer.valueOf(prop.getProperty("MOVEMENT_TIME"));
			
//		    System.out.println(getLocalName()+": Mission length = " + missionLength);
//		    System.out.println(getLocalName()+": Radio range = " + radioRange);
//		    System.out.println(getLocalName()+": Registration window = " + registrationWindow);
//		    System.out.println(getLocalName()+": Analysis time = " + analysisTime);
//		    System.out.println(getLocalName()+": Movement time = " + movementTime);
		    
		    PlatformConstants.MISSION_LENGTH = missionLength;
		    PlatformConstants.RADIO_RANGE = radioRange;
		    PlatformConstants.REGISTRATION_WINDOW = registrationWindow;
		    PlatformConstants.ANALYSIS_TIME = analysisTime;
		    PlatformConstants.MOVEMENT_TIME = movementTime;
		    
		    System.out.println(getLocalName()+": Mission length = " + PlatformConstants.MISSION_LENGTH);
		    System.out.println(getLocalName()+": Radio range = " + PlatformConstants.RADIO_RANGE);
		    System.out.println(getLocalName()+": Registration window = " + PlatformConstants.REGISTRATION_WINDOW);
		    System.out.println(getLocalName()+": Analysis time = " + PlatformConstants.ANALYSIS_TIME);
		    System.out.println(getLocalName()+": Movement time = " + PlatformConstants.MOVEMENT_TIME);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void instantiateTerrainSimulator() {
			ContainerController cc = getContainerController();			
			try {
				AgentController ac = cc.createNewAgent("AgentPlatformSimulator", "org.xploration.team1.platform.AgentPlatformSimulator", argsArray);
				ac.start();
			} catch (Exception e) {
			    e.printStackTrace();
			}			
		}
			
	private File readFile(String fileName) {
			File file = new File(fileName);
		    return file;			
		}
		
	private TreeMap<Integer, TreeMap<Integer, Cell>> createMap(File file){
		TreeMap<Integer, TreeMap<Integer, Cell>> m = new TreeMap<Integer, TreeMap<Integer, Cell>>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
		    String line;
		    int counter = 0;
		    line= br.readLine();
		    if(line != null){
		    	line=line.toLowerCase().replaceAll("[()]","");			    	
		    	String[] grid= line.split(",");			    	
		    	System.out.println(getLocalName()+": Got Map file with grid of: " + grid[0] + " x " + grid[1]);
		    	mapWidth = Integer.parseInt(grid[1]);
		    	mapHeight = Integer.parseInt(grid[0]);
		    }
		    while ((line = br.readLine()) != null) {
		    	if(line != null && !line.isEmpty()) {
		    		counter ++;
		    		int yCoord=0;
		    		line=line.toUpperCase();
		    		String[] minerals = line.split(" ");
		    		TreeMap <Integer, Cell> cells = new TreeMap<Integer, Cell>();
		    		//even rows
		    		if(counter%2==0) {
		    			yCoord = 2;	
		    		}
		    		else {
		    			yCoord = 1;
		    		}
		    		for(int i = 0; i<minerals.length; i++){
	    				Cell c = new Cell();
	    				c.setX(counter);
	    				c.setY(yCoord);
	    				c.setMineral(minerals[i]);
	    				cells.put(yCoord, c);
	    				yCoord+=2;
	    			}
		    		m.put(counter, cells);
		      
		    	}
		    }//going line by line end
		   
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Something wrong with the reader");
			e.printStackTrace();
		}
		return m;
	}
	
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = (PlatformConstants.REGISTRATION_WINDOW + PlatformConstants.MISSION_LENGTH) * 1000;

		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Terminate AgentMapSimulator
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
}
