package org.xploration.team1.platform;


import org.xploration.ontology.CapsuleRegistrationInfo;
import org.xploration.ontology.Cell;
import org.xploration.ontology.CellAnalysis;
import org.xploration.ontology.ClaimCellInfo;
import org.xploration.ontology.MapBroadcastInfo;
import org.xploration.ontology.MovementRequestInfo;
import org.xploration.ontology.RoverRegistrationInfo;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class AgentPlatformSimulator extends Agent {

	private static final long serialVersionUID = 1L;
	
	// Codec for the SL language used and instance of the ontology XplorationOntology that we have created
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    
    // map passed from the map simulator
    private MineralMap mMap; 
    private CompanyInfo mCompanyInfo[];
    
    protected void setup() {
    	
        // Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        
        System.out.println(getLocalName()+": Entered the system.");
        
        // Initialize array of CompanyInfos
        mCompanyInfo = new CompanyInfo[4];
        for (int i=0; i<4; i++) {
        	mCompanyInfo[i] = new CompanyInfo();
        }
        
    	Object[] args = getArguments();
    	
        if (args != null) {
        	if (args[0] instanceof MineralMap) {
        		mMap = (MineralMap) args[0];
	            System.out.println(getLocalName()+": Initialized successfully.");
	            try {
	    			// Creates its own description
	    			DFAgentDescription dfd = new DFAgentDescription();
	    			
	    			// REGISTER SERVICES
	    			ServiceDescription sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.TERRAINSIMULATOR);
	    			dfd.addServices(sd);
	    			
	    			sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.MAPBROADCASTSERVICE);
	    			dfd.addServices(sd);
	    			
	    			sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.MOVEMENTREQUESTSERVICE);
	    			dfd.addServices(sd);
	    			
	    			sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.CAPSULEREGISTRATIONSERVICE);
	    			dfd.addServices(sd);
	    			
	    			sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.ROVERREGISTRATIONSERVICE);
	    			dfd.addServices(sd);
	    			
	    			sd = new ServiceDescription();
	    			sd.setName(this.getName());
	    			sd.setType(org.xploration.ontology.XplorationOntology.RADIOCLAIMSERVICE);
	    			dfd.addServices(sd);
	    			
	    			DFService.register(this, dfd);
	    			// END OF REGISTRATION OF SERVICES ***
	    			
	    			System.out.println(getLocalName()+": Registered in the DF.");
	    			dfd = null;
	    			sd = null;
	    		} 
	    		catch (FIPAException e) {
	    			e.printStackTrace();
	    		}
	            
	            // adding behaviours
	            addBehaviour(new AwaitMapRequestBehaviour(this));
	            addBehaviour(new AwaitCapsuleRegistration(this));
	            addBehaviour(new AwaitRoverRegistration(this));
	            addBehaviour(new AwaitCellAnalysisBehaviour(this));
	            addBehaviour(new AwaitMovementRequestBehaviour(this));
	            addBehaviour(new AwaitMapBroadcastBehaviour(this));
	            addBehaviour(new AwaitCellClaimBehaviour(this));
	            
	            addBehaviour(missionLengthCountdown());
	            
        	}
        } else {
        	System.err.println("ERROR: expected map object.");
        }
	}
    
    private boolean isLegalPosition(Cell cell) {
    	return true;
    }
    
	class AwaitMovementRequestBehaviour extends CyclicBehaviour {
	    	
	    	public AwaitMovementRequestBehaviour(Agent a) { 
	            super(a);  
	        }
	        
	    	private static final long serialVersionUID = 1L;
	    	
			public void action() {
				ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.MOVEMENTREQUESTINFO));
				if(msg != null)
				{
					try
					{
						ContentElement ce = null;
						if (msg.getPerformative() == ACLMessage.REQUEST) {
							
							AID roverAgentAID = msg.getSender();
							
							ce = getContentManager().extractContent(msg);
							if (ce instanceof Action)
							{
								Action agAction = (Action) ce;
								Concept conc = agAction.getAction();
								if (conc instanceof MovementRequestInfo)
								{
									System.out.println(myAgent.getLocalName()+": Received movement request from " + (msg.getSender()).getLocalName());    
	
									MovementRequestInfo mr = (MovementRequestInfo) conc;
									Cell toCell = mr.getCell();
									
									Team teamRequester = new Team();
									
									for (int i=0; i<mCompanyInfo.length; i++) {
										//System.out.println(myAgent.getLocalName()+": " + mCompanyInfo[i].getRoverAID());
										//System.out.println(myAgent.getLocalName()+": " + roverAgentAID);    
										if (mCompanyInfo[i].getRoverAID() != null && mCompanyInfo[i].getRoverAID().equals(roverAgentAID)) {
											teamRequester.setTeamId(mCompanyInfo[i].getTeamId());
										}
									}
									
									Cell currentCell = mCompanyInfo[teamRequester.getTeamId()-1].getRoverPosition();
									
									ACLMessage reply = msg.createReply();
									reply.setLanguage(codec.getName());
									reply.setOntology(ontology.getName());
									reply.setProtocol(msg.getProtocol());
									
									//check if the to-cell is invalid
									//invalid cell condition 1 - to-cell must exist on the map (ex: invalid cell is cell(1,2))
									//invalid cell condition 2 - to-cell must be adjacent to current cell.
									if (mMap.isValidCell(toCell.getX(), toCell.getY())&&isAdjacent(currentCell,toCell)) {
										
										reply.setContent("Agree");
										reply.setPerformative(ACLMessage.AGREE);
										myAgent.send(reply);
										System.out.println(myAgent.getLocalName()+": Agree answer sent for movement request issued by rover of team " + teamRequester.getTeamId());
										
										// execute movement and sending of inform inside of wakeBehaviour
										addBehaviour(executeMovementAfterDelay(teamRequester, roverAgentAID, currentCell, toCell));
										
									} else {
										// if invalid cell, send REFUSE
										reply.setContent("Invalid cell : to-cell is not adjacent to current-cell or to-cell does not exist. current-cell=("+currentCell.getX()+","+currentCell.getY()+") to-cell=("+toCell.getX()+","+toCell.getY()+")");
										reply.setPerformative(ACLMessage.REFUSE);
										myAgent.send(reply);
										System.out.println(myAgent.getLocalName()+": " + reply.getProtocol() + "-" + reply.getPerformative() + " answer sent -> "+ reply.getContent());
										
									}
								}
							}
							
						} else {
							// If what is received is not understood
							System.out.println(myAgent.getLocalName()+": Message received from "+ (msg.getSender()).getLocalName() + " but not understood!");    
							ACLMessage reply = msg.createReply();
							reply.setLanguage(codec.getName());
							reply.setOntology(ontology.getName());
							reply.setProtocol(msg.getProtocol());
												
							//A NOT_UNDERSTOOD is sent		
							reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
							myAgent.send(reply);
							System.out.println(myAgent.getLocalName()+": Not understood message sent");
							
						}
					}
					catch (CodecException e)
					{
						e.printStackTrace();
					}
					catch (OntologyException oe)
					{
						oe.printStackTrace();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					// If no message arrives
					block();
				}  
			}

			private boolean isAdjacent(Cell currentCell, Cell toCell)
			{
				int x = currentCell.getX();
				int y = currentCell.getY();
				
				//center
				Cell[][] adjacentCellMap = new Cell[mMap.getHeight()+4][mMap.getWidth()+4];
				for(int row=2;row<adjacentCellMap.length-2;row++){
					for(int col=2;col<adjacentCellMap[row].length-2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=mMap.getCell(row-1, col-1);						
						}
					}
				}
				
				//left
				for(int row=2;row<adjacentCellMap.length-2;row++) {
					for(int col=0;col<2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row][col+mMap.getWidth()];						
						}
					}
				}
				
				//right
				for(int row=2;row<adjacentCellMap.length-2;row++) {
					for(int col=adjacentCellMap[row].length-2;col<adjacentCellMap[row].length;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row][col-mMap.getWidth()];						
						}
					}
				}
				
				//top
				for(int row=0;row<2;row++) {
					for(int col=2;col<adjacentCellMap[row].length-2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row+mMap.getHeight()][col];						
						}
					}
				}
				
				//bottom
				for(int row=adjacentCellMap.length-2;row<adjacentCellMap.length;row++) {
					for(int col=2;col<adjacentCellMap[row].length-2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row-mMap.getHeight()][col];						
						}
					}
				}
				
				//corner-top-left
				for(int row=0;row<2;row++) {
					for(int col=0;col<2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row+mMap.getHeight()][col+mMap.getWidth()];						
						}
					}
				}
				
				//corner-top-right
				for(int row=0;row<2;row++) {
					for(int col=adjacentCellMap[row].length-2;col<adjacentCellMap[row].length;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row+mMap.getHeight()][col-mMap.getWidth()];						
						}
					}
				}
				
				//corner-bottom-left
				for(int row=adjacentCellMap.length-2;row<adjacentCellMap.length;row++) {
					for(int col=0;col<2;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row-mMap.getHeight()][col+mMap.getWidth()];						
						}
					}
				}
				
				//corner-bottom-right
				for(int row=adjacentCellMap.length-2;row<adjacentCellMap.length;row++) {
					for(int col=adjacentCellMap[row].length-2;col<adjacentCellMap[row].length;col++) {
						if(row%2==col%2) {
							adjacentCellMap[row][col]=adjacentCellMap[row-mMap.getHeight()][col-mMap.getWidth()];						
						}
					}
				}
				
				Cell[] adjacentCells = new Cell[6];
				adjacentCells[0]=adjacentCellMap[x-1][y+1];
				adjacentCells[1]=adjacentCellMap[x][y+2];
				adjacentCells[2]=adjacentCellMap[x+2][y+2];
				adjacentCells[3]=adjacentCellMap[x+3][y+1];
				adjacentCells[4]=adjacentCellMap[x+2][y];
				adjacentCells[5]=adjacentCellMap[x][y];
				
				
				for(int i=0;i<adjacentCells.length;i++){
					if(toCell.getX()==adjacentCells[i].getX()&&toCell.getY()==adjacentCells[i].getY())
						return true;
				}
					
				return false;
			}
	    }
    
	class AwaitCellAnalysisBehaviour extends CyclicBehaviour {
		
		public AwaitCellAnalysisBehaviour(Agent a) { 
	        super(a);  
	    }
	    
		private static final long serialVersionUID = 1L;
		
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.CELLANALYSIS));
			if(msg != null)
			{
				try
				{
					ContentElement ce = null;
					if (msg.getPerformative() == ACLMessage.REQUEST) {
						
						AID roverAgentAID = msg.getSender();
						
						ce = getContentManager().extractContent(msg);
						if (ce instanceof Action)
						{
							Action agAction = (Action) ce;
							Concept conc = agAction.getAction();
							if (conc instanceof CellAnalysis)
							{
								System.out.println(myAgent.getLocalName()+": Received CellAnalysis request from " + (msg.getSender()).getLocalName());    
	
								CellAnalysis ca = (CellAnalysis) conc;
								Cell cell = ca.getCell();
								
								ACLMessage reply = msg.createReply();
								reply.setLanguage(codec.getName());
								reply.setOntology(ontology.getName());
								reply.setProtocol(msg.getProtocol());
								
								//check if the analyzed cell is invalid
								if (mMap.isValidCell(cell.getX(), cell.getY())) {
									// if everything is ok, send AGREE
									reply.setContent("Agree");
									reply.setPerformative(ACLMessage.AGREE);
									myAgent.send(reply);
									System.out.println(myAgent.getLocalName()+": AGREE answer sent.");
									
									// Execute cell analysis behaviour after delay
									addBehaviour(executeCellAnalysisAfterDelay(roverAgentAID, cell));

								} else {
									
									// if invalid cell, send REFUSE
									reply.setContent("Invalid cell ("+cell.getX()+","+cell.getY()+")");
									reply.setPerformative(ACLMessage.REFUSE);
									myAgent.send(reply);
									System.out.println(myAgent.getLocalName()+": REFUSE answer sent -> "+ reply.getContent());
									
								}
							}
						}
						
					} else {
						// If what is received is not understood
						System.out.println(myAgent.getLocalName()+": Message received from "+ (msg.getSender()).getLocalName() + " but not understood!");    
						ACLMessage reply = msg.createReply();
						reply.setLanguage(codec.getName());
						reply.setOntology(ontology.getName());
						reply.setProtocol(msg.getProtocol());
											
						//A NOT_UNDERSTOOD is sent		
						reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
						myAgent.send(reply);
						System.out.println(myAgent.getLocalName()+": Not understood message sent");
						
					}
				}
				catch (CodecException e)
				{
					e.printStackTrace();
				}
				catch (OntologyException oe)
				{
					oe.printStackTrace();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
		
	}
    
	class AwaitMapBroadcastBehaviour extends CyclicBehaviour {
		
		public AwaitMapBroadcastBehaviour(Agent a) { 
	        super(a);  
	    }
	    
		private static final long serialVersionUID = 1L;
		
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.MAPBROADCASTINFO));
			if(msg != null)
			{
				try
				{
					ContentElement ce = null;
					if (msg.getPerformative() == ACLMessage.INFORM) {
						
						AID roverAgent = msg.getSender();
						CompanyInfo senderInfo = new CompanyInfo();
						for (CompanyInfo ci :  mCompanyInfo) {
							if(ci != null) {
				        		if(ci.getRoverAID() != null) {
				        			senderInfo = ci;
				        		}
						}
						}
							
						
						ce = getContentManager().extractContent(msg);
						if (ce instanceof Action)
						{
							Action agAction = (Action) ce;
							Concept conc = agAction.getAction();
							if (conc instanceof MapBroadcastInfo)
							{
								System.out.println(myAgent.getLocalName()+": Received Map Broadcast inform from " + (msg.getSender()).getLocalName());    
	
								MapBroadcastInfo mb = (MapBroadcastInfo) conc;
								org.xploration.ontology.Map receivedMap = mb.getMap();
								
							//getting the location of the Rover who just sent map	
							Cell broadcasterLocation = new Cell();
							AID broadcasterAID = new AID();
							for (CompanyInfo ci :  mCompanyInfo) {
								if(ci !=null){
									if(ci.getRoverAID() !=null){
										if(ci.getRoverAID().toString().equalsIgnoreCase(roverAgent.toString())){
											broadcasterLocation = ci.getRoverPosition();
											broadcasterAID = ci.getRoverAID();
										}
									}
								}
							}
								//iterating in CompanyInfo array to see who is in range and sending map broadcast if in range
								for (CompanyInfo ci :  mCompanyInfo) {
								
						        	if(ci != null) {
						        		//check if rover is in array and if within range
						        		if(ci.getRoverAID() != null) {
						        			//only send if in range and not the sender
						        			if(!ci.getRoverAID().toString().equalsIgnoreCase(roverAgent.toString()) && isWithinRange(broadcasterLocation, ci.getRoverPosition())) {

						        				System.out.println(getLocalName()+": " + ci.getTeamId() + " IS WITHIN RANGE");
						        				
							        			ACLMessage broadcastReply = new ACLMessage(ACLMessage.INFORM);
							        			broadcastReply.addReceiver(ci.getRoverAID());
							        			broadcastReply.setLanguage(codec.getName());
							        			broadcastReply.setOntology(ontology.getName());
												//IMPORTANT STEP
							        			broadcastReply.setProtocol(XplorationOntology.MAPBROADCASTINFO);
												MapBroadcastInfo mapBroadcastInfo = new MapBroadcastInfo();
												mapBroadcastInfo.setMap(receivedMap);
												// As it is an action and the encoding language the SL, it must be wrapped into an Action
												Action broadcastAction = new Action(ci.getRoverAID(), mapBroadcastInfo);
												try
												{
													// The ContentManager transforms the java objects into strings
													getContentManager().fillContent(broadcastReply, broadcastAction);
													send(broadcastReply);
													System.out.println(getLocalName()+": sent Map Broadcast to rover of team " + ci.getTeamId());
												
												}
												catch (Exception e)
												{
													e.printStackTrace();
												}
						        			}
						        		}
						        	}
						      
						        } //END FOR LOOP
							
							}
						}
						
					} 
				}
				catch (CodecException e)
				{
					e.printStackTrace();
				}
				catch (OntologyException oe)
				{
					oe.printStackTrace();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
	}
    
	class AwaitCellClaimBehaviour extends CyclicBehaviour {
		
		public AwaitCellClaimBehaviour(Agent a) { 
	        super(a);  
	    }
	    
		private static final long serialVersionUID = 1L;
		
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO));
			if(msg != null)
			{
				try
				{
					ContentElement ce = null;
					if (msg.getPerformative() == ACLMessage.INFORM) {
						
						AID roverAgent = msg.getSender();
						
						ce = getContentManager().extractContent(msg);
						if (ce instanceof Action)
						{
							Action agAction = (Action) ce;
							Concept conc = agAction.getAction();
							if (conc instanceof ClaimCellInfo)
							{
								System.out.println(myAgent.getLocalName()+": Received CLAIMCELLINFO from " + (msg.getSender()).getLocalName());    
	
								ClaimCellInfo cc = (ClaimCellInfo) conc;
								org.xploration.ontology.Map cellsToClaim = cc.getMap();
								int claimerId = cc.getTeam().getTeamId();
								
								//getting the location of the Rover who just claim the cell
								Cell roverLocation = getRoverLocationFromRoverAID(roverAgent);
							
								CompanyInfo ci = mCompanyInfo[claimerId-1];
				        		if(ci.getRoverAID() != null) {
				        			//only send ClaimCellInfo if the capsule is in range.
				        			Cell capsuleLocation = ci.getCapsulePosition();
				        			//System.out.println("CLAIMCELLINFO:rover("+roverLocation.getX()+","+roverLocation.getY()+") capsuleLocation("+capsuleLocation.getX()+","+capsuleLocation.getY()+") isWithinRange="+isWithinRange(roverLocation, capsuleLocation));
				        			if(isWithinRange(roverLocation, capsuleLocation)) {
				        
				        				//System.out.println(getLocalName()+": Capsule" + ci.getTeamId() + "("+capsuleLocation.getX()+","+capsuleLocation.getY()+") IS WITHIN RANGE of Rover"+ ci.getTeamId() + "("+roverLocation.getX()+","+roverLocation.getY()+")");
				        				
					        			ACLMessage ClaimCellMsg = new ACLMessage(ACLMessage.INFORM);
					        			ClaimCellMsg.addReceiver(ci.getCapsuleAID());
					        			ClaimCellMsg.setLanguage(codec.getName());
					        			ClaimCellMsg.setOntology(ontology.getName());
					        			ClaimCellMsg.setProtocol(XplorationOntology.CLAIMCELLINFO);
										ClaimCellInfo cci = new ClaimCellInfo();
										cci.setMap(cellsToClaim);
										Team t = new Team();
										t.setTeamId(claimerId);
										cci.setTeam(t);
										
										// As it is an action and the encoding language the SL, it must be wrapped into an Action
										Action action = new Action(ci.getCapsuleAID(), cci);
										try
										{
											// The ContentManager transforms the java objects into strings
											getContentManager().fillContent(ClaimCellMsg, action);
											send(ClaimCellMsg);
											System.out.println(getLocalName()+": sent CLAIMCELLINFO to capsule of team" + claimerId);
										
										}
										catch (Exception e)
										{
											e.printStackTrace();
										}
				        			}
				        		}
						
							}
						}
					
					} 
				}
				catch (CodecException e)
				{
					e.printStackTrace();
				}
				catch (OntologyException oe)
				{
					oe.printStackTrace();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
					block();
				}  
		}
	}
    
	private Cell getRoverLocationFromRoverAID(AID roverAID) {
		Cell roverLocation = new Cell();
		for (CompanyInfo ci :  mCompanyInfo) {
			if(ci !=null){
				if(ci.getRoverAID() !=null){
					if(ci.getRoverAID().toString().equalsIgnoreCase(roverAID.toString())){
						roverLocation = ci.getRoverPosition();
					}
				}
			}
		}
		return roverLocation;
	}
    
	private boolean isWithinRange(Cell broadcastLocation, Cell receiverLocation) {
		
		int x = broadcastLocation.getX();
		int y = broadcastLocation.getY();
		int recX = receiverLocation.getX();
		int recY = receiverLocation.getY();
		
		//if broadcastLocation==receiverLocation, return true
		if(x==recX&&y==recY) return true;
		
		//vertical down
		for(int i = 1; i<=PlatformConstants.RADIO_RANGE; i++){
			int cellX = x+(i*2);
			if(cellX > mMap.getHeight()) cellX = cellX -mMap.getHeight();
			
			if(recX == cellX && recY == y) {
				return true;
			}
		}
		//vertical up 
		for (int i = 1; i<=PlatformConstants.RADIO_RANGE; i++){
			int cellX = x-(i*2);
			if (cellX <= 0) cellX = mMap.getHeight() + cellX;
			
			if(recX == cellX && recY == y) {
				return true;
			}
		}
		
		//diagonal left up
		for(int i = 1; i<=PlatformConstants.RADIO_RANGE; i++){
			int cellX = x-i*1;
			int cellY =  y-i*1;
			if(cellX <= 0) cellX = mMap.getHeight() + cellX;
			if(cellY <= 0 ) cellY = mMap.getWidth() + cellY;
			
			if(recX == cellX && recY == cellY) {
				return true;
			}
		}
		
		//diagonal right up 
		for(int i = 1; i<=PlatformConstants.RADIO_RANGE;i++){
			int cellX = x-i*1;
			int cellY = y+i*1;
			if(cellX <= 0) cellX = mMap.getHeight() + cellX;
			if(cellY > mMap.getWidth()) cellY = cellY -mMap.getWidth();
		
			if(recX ==cellX  && recY == cellY ) {
				return true;
			}
		}
		
		//diagonal left down 
		for(int i = 1; i<=PlatformConstants.RADIO_RANGE;i++){
			int cellX = x+i*1;
			int cellY = y-i*1;
			if(cellX > mMap.getHeight()) cellX = cellX - mMap.getHeight();
			if(cellY <= 0 ) cellY = mMap.getWidth() + cellY;
			
			if(recX == cellX && recY == cellY) {
				return true;
			}
		}
		
		//diagonal right down 
		for(int i = 1; i<=PlatformConstants.RADIO_RANGE;i++){
			int cellX = x+i*1;
			int cellY =  y+i*1;
			if(cellX > mMap.getHeight()) cellX = cellX - mMap.getHeight();
			if(cellY > mMap.getWidth()) cellY = cellY -mMap.getWidth();
			
			if(recX ==cellX  && recY == cellY) {
				return true;
			}
		}
		return false;
	}

	class AwaitMapRequestBehaviour extends SimpleBehaviour {
	
		public AwaitMapRequestBehaviour(Agent a) { 
	        super(a);  
	    }
	    
		boolean requestSent = false;
		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			ACLMessage msg = blockingReceive(MessageTemplate.MatchProtocol(PlatformConstants.MAP_REQUEST));
			//ACLMessage msg = receive();
			if(msg != null)
			{
				try
				{
					Object content = msg.getContentObject();
					if(msg.getPerformative() == ACLMessage.REQUEST) {
						if(content instanceof MapRequest)
							System.out.println(myAgent.getLocalName()+": Received MapRequest from " + (msg.getSender()).getLocalName());    
							ACLMessage reply = msg.createReply();
							reply.setLanguage(codec.getName());
							reply.setOntology(ontology.getName());
							reply.setProtocol(msg.getProtocol());
							reply.setPerformative(ACLMessage.INFORM);
							MapRequest mapRequest = new MapRequest();
							mapRequest.setMap(mMap);
							try
							{
								reply.setContentObject(mapRequest);
								myAgent.send(reply);
								System.out.println(getLocalName()+": sent MapRequest INFORM reply. (Map dimension: "+mMap.getWidth()+"x"+mMap.getHeight()+")");
								requestSent = true;
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
		@Override
		public boolean done() 
		{
			return requestSent;
		}
	}
      
	class AwaitCapsuleRegistration extends CyclicBehaviour {
	
		public AwaitCapsuleRegistration(Agent a) { 
	        super(a);  
	    }
	    
		boolean registrationReceived = false;
		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.CAPSULEREGISTRATIONINFO));
			if(msg != null)
			{
				ContentElement ce = null;
				try
				{
					if (msg.getPerformative() == ACLMessage.INFORM) {
						ce = getContentManager().extractContent(msg);
						if(ce instanceof Action) {
	                        Action agAction = (Action) ce;
	                        Concept concept = agAction.getAction();
							if(concept instanceof CapsuleRegistrationInfo) {
								System.out.println(myAgent.getLocalName()+": Received Capsule Registration from " + (msg.getSender()).getLocalName());
								
								Cell capsuleCellLocation = ((CapsuleRegistrationInfo) concept).getCell();
								int capsuleTeamID = ((CapsuleRegistrationInfo) concept).getTeam().getTeamId();
								AID capsuleAID = msg.getSender();
								
								System.out.println(myAgent.getLocalName()+": Registered capsule with teamID: " + capsuleTeamID + " and capsuleCellLocation: " + capsuleCellLocation.getX() + "," + capsuleCellLocation.getY());
								
								mCompanyInfo[capsuleTeamID-1].setTeamID(capsuleTeamID);
								mCompanyInfo[capsuleTeamID-1].setCapsuleAID(capsuleAID);
								mCompanyInfo[capsuleTeamID-1].setCapsulePosition(capsuleCellLocation);
								
								registrationReceived = true;
							}
						}
					}
	
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
		
	}
    
	class AwaitRoverRegistration extends CyclicBehaviour {
		public AwaitRoverRegistration(Agent a) { 
	        super(a);  
	    }
	    
		boolean registrationReceived = false;
		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.ROVERREGISTRATIONINFO));
			if(msg != null)
			{
				ContentElement ce;
				try
				{
					ce = getContentManager().extractContent(msg);
					if(ce instanceof Action) {
						Action agAction = (Action) ce;
	                    Concept concept = agAction.getAction();
						if(concept instanceof RoverRegistrationInfo) {
							System.out.println(myAgent.getLocalName()+": Received Rover Registration from " + (msg.getSender()).getLocalName());
							
							Cell roverCellLocation = ((RoverRegistrationInfo) concept).getCell();
							int roverTeamID = ((RoverRegistrationInfo) concept).getTeam().getTeamId();
							AID roverAID = msg.getSender();
							
							System.out.println(myAgent.getLocalName()+": Registered rover with teamID: " + roverTeamID + " and roverCellLocation: " + roverCellLocation.getX() + "," + roverCellLocation.getY());
							
							mCompanyInfo[roverTeamID-1].setRoverAID(roverAID);
							mCompanyInfo[roverTeamID-1].setRoverPosition(roverCellLocation);
							
							registrationReceived = true;
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
	}
    
	private Behaviour executeMovementAfterDelay(Team teamRequester, AID roverAgentAID, Cell currentCell, Cell toCell) {
		int movementTimeInMiliseconds = PlatformConstants.MOVEMENT_TIME * 1000;
		
		return new WakerBehaviour(this, movementTimeInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				 ACLMessage informSuccess = new ACLMessage(ACLMessage.INFORM);
					informSuccess.addReceiver(roverAgentAID);
					informSuccess.setLanguage(codec.getName());
					informSuccess.setOntology(ontology.getName());
					informSuccess.setProtocol(XplorationOntology.MOVEMENTREQUESTINFO);
					
					//Action informSuccessAction = new Action(roverAgent, new MovementRequestInfo());
					try
					{
						// The ContentManager transforms the java objects into strings
						//getContentManager().fillContent(informSuccess, informSuccessAction);
						send(informSuccess);
						
						//update Rover Location
						mCompanyInfo[teamRequester.getTeamId()-1].setRoverPosition(toCell);
						System.out.println(myAgent.getLocalName()+ ": Rover"+ teamRequester.getTeamId()+" is moving from ("+currentCell.getX()+","+currentCell.getY()+") to ("+mCompanyInfo[teamRequester.getTeamId()-1].getRoverPosition().getX()+","+mCompanyInfo[teamRequester.getTeamId()-1].getRoverPosition().getY()+") ");
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					System.out.println(myAgent.getLocalName()+": Current rover position of Team"+teamRequester.getTeamId()+" is ("+mCompanyInfo[teamRequester.getTeamId()-1].getRoverPosition().getX()+","+mCompanyInfo[teamRequester.getTeamId()-1].getRoverPosition().getY()+")");

			 }
		};
	}
	
	private Behaviour executeCellAnalysisAfterDelay(AID roverAgentAID, Cell cell) {
		int cellAnalysisTimeInMiliseconds = PlatformConstants.ANALYSIS_TIME * 1000;
		
		return new WakerBehaviour(this, cellAnalysisTimeInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				
				if (isLegalPosition(cell)) {
					// if legal position, send INFORM
					Cell cellToSend = new Cell();
					cellToSend = mMap.getCell(cell.getX(),cell.getY());
				
					CellAnalysis cellAnalysisToInform = new CellAnalysis();
					cellAnalysisToInform.setCell(cellToSend);
					
					ACLMessage informSuccess = new ACLMessage(ACLMessage.INFORM);
					informSuccess.addReceiver(roverAgentAID);
					informSuccess.setLanguage(codec.getName());
					informSuccess.setOntology(ontology.getName());
					informSuccess.setProtocol(XplorationOntology.CELLANALYSIS);
					
					Action informSuccessAction = new Action(roverAgentAID, cellAnalysisToInform);
					try
					{
						// The ContentManager transforms the java objects into strings
						getContentManager().fillContent(informSuccess, informSuccessAction);
						send(informSuccess);
						System.out.println(myAgent.getLocalName()+": SUCCESS answer sent -> "+ "cell("+cellToSend.getX()+","+cellToSend.getY()+","+ cellToSend.getMineral() + ")");
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}										
				} else {
					// if illegal position, send FAILURE
					ACLMessage failureReply = new ACLMessage(ACLMessage.FAILURE);
					failureReply.addReceiver(roverAgentAID);
					failureReply.setLanguage(codec.getName());
					failureReply.setOntology(ontology.getName());
					failureReply.setProtocol(XplorationOntology.CELLANALYSIS);
					failureReply.setContent("cell("+cell.getX()+","+cell.getY()+")"+" requested to be analyzed illegaly");
					myAgent.send(failureReply);
					System.out.println(myAgent.getLocalName()+": FAILURE answer sent -> "+ failureReply.getContent());
				}
			 }
		};
	}
	
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = (PlatformConstants.REGISTRATION_WINDOW + PlatformConstants.MISSION_LENGTH) * 1000;
		// After mission length expires -> print score and terminate platform agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Terminate AgentPlatformSimulator
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
	
}


