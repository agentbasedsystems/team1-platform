package org.xploration.team1.platform;

public class PlatformConstants {
	
	public static final String MAP_SIMULATOR_SERVICE_NAME = "mapSimulator";
	public static final String MAP_REQUEST = "MapRequest";
	
	//configuration file constants
    public static int MISSION_LENGTH = 0; // To be read from configuration file, the value will be changed
    public static int RADIO_RANGE = 0; // To be read from configuration file, the value will be changed
    public static int REGISTRATION_WINDOW = 0; // To be read from configuration file, the value will be changed
    public static int ANALYSIS_TIME = 0; // To be read from configuration file, the value will be changed
    public static int MOVEMENT_TIME = 0; // To be read from configuration file, the value will be changed
}
