package org.xploration.team1.platform;

import java.util.ArrayList;

import org.xploration.ontology.Cell;

public class ScoreCard {
	
	int teamId;
	ArrayList<Cell> correctCells;
	ArrayList<Cell> wrongCells;
	ArrayList<Cell> lateCells;
	int finalScore;
	
	public ScoreCard(int id){
		this.teamId = id;
		correctCells = new ArrayList<Cell>();
		wrongCells = new ArrayList<Cell>();
		lateCells = new ArrayList<Cell>();
		finalScore = 0;
	}
	
	public int getTeamId() {
		return this.teamId;
	}
	
	public ArrayList<Cell> getCorrectCells(){
	 return correctCells;	
	}
	
	public ArrayList<Cell> getWrongCells(){
		 return wrongCells;	
		}
	
	public ArrayList<Cell> getLateCells(){
		 return lateCells;	
		}
	
	public int getFinalScore(){
		return finalScore;
	}
	
	public void setTeamId(int i) {
		this.teamId = i;
	}
	
	public void setFinalScore(int i) {
		this.finalScore = i;
	}
	
	public boolean alreadyClaimed(Cell cellToClaim){
		if(alreadyInList(this.correctCells, cellToClaim)) 
			return true;
		else
			return false;
	}
	
	
	
	public void addCorrectCell(Cell c){
		if(!alreadyInList(this.correctCells, c))
			this.correctCells.add(c);	
		}
	
	
	public void addWrongCell(Cell w){
		if(!alreadyInList(this.wrongCells, w))
			this.wrongCells.add(w);	
		}
	
	
	public void addLateCell(Cell l){
		if(!alreadyInList(this.lateCells, l))
			this.lateCells.add(l);	
		}
	
	private boolean alreadyInList(ArrayList<Cell> l, Cell c){
		for(Cell i : l){
			if(i.getX() == c.getX() && i.getY() == c.getY() && i.getMineral().equalsIgnoreCase(c.getMineral())) return true;
		}
		return false;
	}
	
	
	
	public String toString() {
		return "Team " + this.teamId + ": TOTAL SCORE=" + this.finalScore + " (#correct=" + this.correctCells.size() + ", #wrong=" + this.wrongCells.size() + ", #late=" + this.lateCells.size()+")";
	}

}
