package org.xploration.team1.platform;

import org.xploration.ontology.Cell;

import jade.core.AID;

public class CompanyInfo {
	
    private int mTeamID;
	
    private Cell mRoverPosition;
    private AID mRoverAID;
    
    private Cell mCapsulePosition;
    private AID mCapsuleAID;

	public CompanyInfo(){
		mTeamID = 0;
		mRoverPosition = null;
		mRoverAID = null;
		mCapsulePosition = null;
		mCapsuleAID = null;	
	}
	
	public CompanyInfo(int teamID, Cell roverPosition, AID roverAID, Cell capsulePosition, AID capsuleAID){
		mTeamID = teamID;
		mRoverPosition = roverPosition;
		mRoverAID = roverAID;
		mCapsulePosition = capsulePosition;
		mCapsuleAID = capsuleAID;	
	}
	
	public void setTeamID(int teamID) {
		mTeamID = teamID;
	}
	
	public int getTeamId() {
		return mTeamID;
	}
	
	public void setRoverPosition(Cell roverPosition) {
		mRoverPosition = roverPosition;
	}
	
	public Cell getRoverPosition() {
		return mRoverPosition;
	}
	
	public void setRoverAID(AID roverAID) {
		mRoverAID = roverAID;
	}
	
	public AID getRoverAID() {
		return mRoverAID;
	}
	
	public void setCapsulePosition(Cell capsulePosition) {
		mCapsulePosition = capsulePosition;
	}
	
	public Cell getCapsulePosition() {
		return mCapsulePosition;
	}
	
	public void setCapsuleAID(AID capsuleAID) {
		mCapsuleAID = capsuleAID;
	}
	
	public AID getCapsuleAID() {
		return mCapsuleAID;
	}

}
