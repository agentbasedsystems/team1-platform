package org.xploration.team1.platform;

import org.xploration.team1.platform.MineralMap;

public class MapRequest implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private MineralMap map;
	public void setMap(MineralMap value) { 
		this.map=value;
	}
	public MineralMap getMap() {
		return this.map;
	}
}
