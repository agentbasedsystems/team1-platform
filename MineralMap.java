package org.xploration.team1.platform;

import java.util.TreeMap;
import java.util.Map.Entry;

import org.xploration.ontology.Cell;

import jade.content.Concept;

public class MineralMap implements Concept {

	private static final long serialVersionUID = 1L;
	
	private TreeMap<Integer, TreeMap<Integer, Cell>> map;
	private int height;
	private int width;
	
	//TreeMap implementation constructor
	public MineralMap(TreeMap<Integer, TreeMap<Integer, Cell>> otherMap, int h, int w){
		this.map = new TreeMap<Integer, TreeMap<Integer,Cell>>();
		this.map.putAll(otherMap);
		this.height = h;
		this.width = w;
		
	}
	
	//TreeMap implementation setters-getters
	
	public TreeMap<Integer, TreeMap<Integer, Cell>> getMap() {
		return this.map;
	}
	
	public void setMap(TreeMap<Integer, TreeMap<Integer, Cell>> otherMap) {
		this.map.putAll(otherMap);
	}
	
	public boolean isValidCell(int x, int y) {
		if(this.map.containsKey(x)){
			if(this.map.get(x).containsKey(y)){
				return true;
			}
		}
		return false;
	}
	
	public Cell getCell(int x, int y) {
		if(isValidCell(x, y)) {
			//System.out.println("we are about to return cell with coords: " + this.getMap().get(x).get(y).getX() + " and " + this.getMap().get(x).get(y).getY() + " and mineral " + this.getMap().get(x).get(y).getMineral());
			return this.getMap().get(x).get(y);
		}
		else return null;
	}
	
	public void setWidth(int w){
		this.width = w;
	}
	
	public void setHeight(int h) {
		this.height = h;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public boolean setCell(Cell cell) {
		return true;
	}
	
	public void printMap(){
		for (Entry<Integer, TreeMap<Integer, Cell>> e1 : map.entrySet()) {
		     System.out.println("Row: " + e1.getKey() + " with cells:");
		     for(Entry<Integer, Cell> e : e1.getValue().entrySet()) {
		    	 System.out.println("(" + e1.getKey() + ", " + e.getKey() + ") -> mineral " + e.getValue().getMineral());
		     }
		}
	}
}