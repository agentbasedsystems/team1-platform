package org.xploration.team1.platform;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

import org.xploration.ontology.Cell;
import org.xploration.ontology.ClaimCellInfo;
import org.xploration.ontology.RegistrationRequest;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

public class AgentSpacecraft extends Agent {
	
	private static final long serialVersionUID = 1L;
	
	// Codec for the SL language used and instance of the ontology XplorationOntology that we have created
	private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private AID terrainSimulatorAID = new AID();
    private int mMissionLength; // number which represents seconds of mission length
    private int mRadioRange; // number which represents number of cells for radio range
    private MineralMap mineralMap; 
    private TreeMap<Integer, ScoreCard> scores;
    
	// Array of integers which will be filled with registered team IDs
	private Integer[] mTeams = new Integer[4];
	
	// Array of integers which will be filled with Map Dimensions
	private Integer[] mMapDimensions = new Integer[2];
	
	// Array of coordinates (int x, int j) where each team's capsule will be located
	private Integer[][] mCapsuleCoordinates = new Integer[4][2];
	
	private boolean registrationWindowOpen; 
	
	protected void setup() {
		System.out.println(getLocalName()+": Entered the system.");
		 //initializing arraylist of ScoreCards
        scores = new TreeMap<Integer, ScoreCard>();
        for(int i = 0; i<4; i++){
        	scores.put(i+1, new ScoreCard(i+1));
        }
		
		// Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

		try {
			DFAgentDescription dfd = new DFAgentDescription();
			
			ServiceDescription sd = new ServiceDescription();
			sd.setName(this.getName());
			sd.setType(XplorationOntology.REGISTRATIONDESK);
			dfd.addServices(sd);
			
			sd = new ServiceDescription();
			sd.setName(this.getName());
			sd.setType(XplorationOntology.SPACECRAFTCLAIMSERVICE);
			dfd.addServices(sd);
			
			DFService.register(this, dfd);
			
			System.out.println(getLocalName()+": Registered in the DF.");
			dfd = null;
			sd = null;
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		// Fill in array of registered teams with zeros
		for (int i=0; i<mTeams.length; i++) {
			// Initialize teamIDs to 0
			mTeams[i] = 0;
			// Initialize team capsule's coordinates do 0,0
			mCapsuleCoordinates[i][0] = 0;
			mCapsuleCoordinates[i][1] = 0;
		}
		
		// Get map dimensions from MapSimulator (N, M) and load them into global array mMapDimensions
		getMapDimensions();
		
		
		// TODO: Read into variables
		// wait some time so that we assure that config file constants are read!
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// **************
		
		registrationWindowOpen = true;
		addBehaviour(registrationTimeCountdown());
		receiveRegistrationRequests();
	}

	private Behaviour registrationTimeCountdown() {
		
		int registrationTimeMiliseconds = PlatformConstants.REGISTRATION_WINDOW * 1000;
		
		// After registration time expires -> Do not allow new registrations and start the mission
		return new WakerBehaviour(this, registrationTimeMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				registrationWindowOpen = false;
				System.out.println(myAgent.getLocalName()+": ******************** \n Registration time of " + PlatformConstants.REGISTRATION_WINDOW + " seconds has passed!\n" + " ******************** "); 
				System.out.println(myAgent.getLocalName()+": ******************** \n Mission of " + PlatformConstants.MISSION_LENGTH + " seconds starts!\n" + " ******************** "); 
				addBehaviour(missionLengthCountdown());
				
				
				// Instantiate all the registered companies' capsules
				for (int i= 0; i < mTeams.length; i++) {
					if (mTeams[i] != 0) {
						instantiateCapsule(mTeams[i]);
					}
				}
			 }
		};
	}

	private void receiveRegistrationRequests() {
		addBehaviour(new CyclicBehaviour(this)
		{
			private static final long serialVersionUID = 1L;

			public void action()
			{	
				if(mMapDimensions[0]!=0 && mMapDimensions[1]!=0) {
					// Waits for registration requests
					ACLMessage msg = receive(MessageTemplate.MatchProtocol(XplorationOntology.REGISTRATIONREQUEST));
					if(msg != null)
					{
						try
						{
							ContentElement ce = null;
							if (msg.getPerformative() == ACLMessage.REQUEST)
							{
								// If an registration request arrives (type REQUEST)
								// The ContentManager transforms the message content (string) in java objects
								ce = getContentManager().extractContent(msg);
								// We expect an action inside the message
								if (ce instanceof Action)
								{
									Action agAction = (Action) ce;
									Concept conc = agAction.getAction();
									// If the action is RegistrationRequest...
									if (conc instanceof RegistrationRequest)
									{
										System.out.println(myAgent.getLocalName()+": Received RegistrationRequest from " + (msg.getSender()).getLocalName());    
										ACLMessage reply = msg.createReply();
	
										reply.setLanguage(codec.getName());
										reply.setOntology(ontology.getName());
										reply.setProtocol(msg.getProtocol());
	
										if (registrationWindowOpen == false) 
										{
											// If registration time has passed -> REFUSE
											reply.setContent("Too late");
											reply.setPerformative(ACLMessage.REFUSE);
											myAgent.send(reply);
											System.out.println(myAgent.getLocalName()+": Refuse answer sent -> "+ reply.getContent());
										}
										else 
										{
											// If everything OK -> AGREE
											reply.setContent("Agree");
											reply.setPerformative(ACLMessage.AGREE);
											myAgent.send(reply);
											System.out.println(myAgent.getLocalName()+": Agree answer sent -> "+ reply.getContent());
											
											// get teamID and put it into Array of registered teams (mTeams)
											RegistrationRequest registrationRequest = (RegistrationRequest) conc;
											Team team = registrationRequest.getTeam();
											
											int teamID = team.getTeamId();
											
											if (mTeams[teamID-1] == teamID) 
											{
												// SEND failure
												reply.setContent("Company with this ID is already registered.");
												reply.setPerformative(ACLMessage.FAILURE);
												myAgent.send(reply);
												System.out.println(myAgent.getLocalName()+": Failure answer sent -> "+ reply.getContent());
												
											}
											else 
											{
												mTeams[teamID-1] = teamID;
												// SEND inform -> success
												reply.setContent("Company " + teamID + " successfuly registered.");
												reply.setPerformative(ACLMessage.INFORM);
												myAgent.send(reply);
												System.out.println(myAgent.getLocalName()+": Success answer sent -> "+ reply.getContent());
							
												// assign random location to capsuleX (check if it is not already assigned)
												assignCellToCapsule(teamID);
											}
										}
									}
								}
							}
	
							else
							{
								// If what is received is not understood
								System.out.println(myAgent.getLocalName()+": Message received from "+ (msg.getSender()).getLocalName() + " but not understood!");    
								ACLMessage reply = msg.createReply();
								reply.setLanguage(codec.getName());
								reply.setOntology(ontology.getName());
								reply.setProtocol(msg.getProtocol());
													
								//A NOT_UNDERSTOOD is sent		
								reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
								myAgent.send(reply);
								System.out.println(myAgent.getLocalName()+": Not understood message sent");
							}
						}
						catch (CodecException e)
						{
							e.printStackTrace();
						}
						catch (OntologyException oe)
						{
							oe.printStackTrace();
						}
					}
					else
					{
						// If no message arrives
						block();
					}
				}     
			}
		});
		
	}
	
	private void assignCellToCapsule(int teamID) {
		
		boolean cellOccupied = true;
		
		Integer N = mMapDimensions[0];
		Integer M = mMapDimensions[1];
		
		Integer x = 0;
		Integer y = 0;
		
		while (cellOccupied == true) {
			 x = ThreadLocalRandom.current().nextInt(1, N+1);
			 y = ThreadLocalRandom.current().nextInt(1, M+1);
			 
			// simple algorithm to assure valid cell indexes
			if ((x+y) % 2 == 1) {
				if (x>=2) {
					x = x-1;
				} else {
					x = x+1;
				}
			}
			// x and y are now valid indexes of a cell
			// now we need to check if those cell coordinates are already assigned to other capsules
			int counter = 0;
			for (int i=0; i<mTeams.length; i++) {
				if (!((mCapsuleCoordinates[i][0] == x) && (mCapsuleCoordinates[i][1] == y))) {
					counter++; // count to mTeams.length (4)
				}
			}
			
			if (counter == mTeams.length) {
				cellOccupied = false;
			}
		}
		
		mCapsuleCoordinates[teamID-1][0] = x;
		mCapsuleCoordinates[teamID-1][1] = y;
	}

	private void instantiateCapsule(int teamID) {
		
        mMissionLength = PlatformConstants.MISSION_LENGTH;
        mRadioRange = PlatformConstants.RADIO_RANGE;
        
		Integer[] agentCapsuleArguments = new Integer[6];
		agentCapsuleArguments[0] = mCapsuleCoordinates[teamID-1][0]; // x
		agentCapsuleArguments[1] = mCapsuleCoordinates[teamID-1][1]; // y
		agentCapsuleArguments[2] = mMapDimensions[0]; // N
		agentCapsuleArguments[3] = mMapDimensions[1]; // M
		agentCapsuleArguments[4] = mMissionLength; // missionLength
		agentCapsuleArguments[5] = mRadioRange;
		
		AgentContainer mainContainer = getContainerController();
		
		try {
			AgentController agentController = mainContainer.createNewAgent("AgentCapsule" + teamID, "org.xploration.team" + teamID + ".company.Capsule" + teamID, agentCapsuleArguments);
			agentController.start();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

	private void getMapDimensions() {
		mMapDimensions[0] = 0;
		mMapDimensions[1] = 0;
		
		// GET MAP FROM TERRAIN SIMULATOR BEHAVIOUR
		addBehaviour(new SimpleBehaviour(this)
		{
			boolean requestSent = false;
			
			private static final long serialVersionUID = 1L;

			@Override
			public void action() {
				
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(XplorationOntology.TERRAINSIMULATOR);
				dfd.addServices(sd);
				
				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[1];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0)
					{
						terrainSimulatorAID = (AID) res[0].getName();
						// Send request for cell analysis to the terrainSimulator
						ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
						msg.addReceiver(terrainSimulatorAID);
						
						//IMPORTANT STEP
						msg.setProtocol(PlatformConstants.MAP_REQUEST);
						
						MapRequest mapRequest = new MapRequest();
						mapRequest.setMap(null);
						
						try
						{
							// The ContentManager transforms the java objects into strings
							msg.setContentObject(mapRequest);
							send(msg);
							System.out.println(getLocalName()+": Map request sent!");
							requestSent = true;
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public boolean done() 
			{
				return requestSent;
			}
			
		});
		
		// awaits for responses from TerrainSimulator
		addBehaviour(new CyclicBehaviour(this)
		{
			private static final long serialVersionUID = 1L;

			public void action()
			{
				// Waits for REFUSE, NOT UNDERSTOOD OR AGREE
				ACLMessage msg = receive(MessageTemplate.MatchSender(terrainSimulatorAID));
				if(msg != null) {
					try {
							if (msg.getPerformative() == ACLMessage.INFORM) {
								// If what is received is INFORM SUCCESS
								System.out.println(myAgent.getLocalName()+": Message form "+(msg.getSender()).getLocalName() + " INFORM (MapRequest)");
								Object content = msg.getContentObject();
								if (content instanceof MapRequest)
								{
									MapRequest mapRequest = (MapRequest) content;
									MineralMap map = mapRequest.getMap();
									mineralMap = mapRequest.getMap();
									mMapDimensions[0] = map.getHeight();
									mMapDimensions[1] = map.getWidth();
									System.out.println(myAgent.getLocalName()+": Map received: " + " N: " + mMapDimensions[0] + " M: " + mMapDimensions[1]);
								}
							} else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
								// If what is received is NOT UNDERSTOOD
								System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " NOT UNDERSTOOD");
							} else {
								System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " received but it is neither INFORM, nor NOT_UNDERSTOOD.");
							}
					} catch (Exception e) {
						e.printStackTrace();
					} 
				} else {
					// If no message arrives
					block();
				}
			}
		});
		
		// awaits for CLAIMCELLINFO from capsule
		addBehaviour(new CyclicBehaviour(this)
		{
			private static final long serialVersionUID = 1L;

			public void action()
			{
		    	ACLMessage msg = receive(
							MessageTemplate.and(MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO),
							MessageTemplate.and(
							MessageTemplate.MatchLanguage(codec.getName()), 
							MessageTemplate.MatchOntology(ontology.getName()))));
	    		if(msg != null){
					ContentElement ce = null;
					try
					{
						if (msg.getPerformative() == ACLMessage.INFORM) {
							ce = getContentManager().extractContent(msg);
							if(ce instanceof Action) {
		                        Action agAction = (Action) ce;
		                        Concept conc = agAction.getAction();
								if(conc instanceof ClaimCellInfo) {
									ClaimCellInfo cc = (ClaimCellInfo) conc;
									
									System.out.println(myAgent.getLocalName()+": Received Claim Cell Info from " + (msg.getSender()).getLocalName());
									//adding scores
									addScores(cc);
									
								
								}
							}

						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					// If no message arrives
					block();
				}
			}
		});
	}
	
	private void addScores(ClaimCellInfo cc){
		//gets claimCellInfo and adds correct, wrong or late cells to Scorecards
		int claimerId = cc.getTeam().getTeamId();
		@SuppressWarnings("unchecked")
		Iterator<Cell> iterator = cc.getMap().getAllCellList();
		//iterate through received cells and do the biz
		while (iterator.hasNext()) {
			Cell claimedCell = iterator.next();
			
			//wrong mineral -> woe is you! 
			if(!correctMineral(claimedCell, mineralMap.getCell(claimedCell.getX(), claimedCell.getY()))){
				scores.get(claimerId).addWrongCell(claimedCell);
			}
			//whew! correct mineral
			else { 
				//lets see if we are on time or late
				boolean claimed= false;
				for(Entry<Integer, ScoreCard> teamCard : scores.entrySet()){
				//see other teams' lists
					if(teamCard.getKey()!=claimerId){
					//somebody already claimed -> adding to claimer's late
						if(teamCard.getValue().alreadyClaimed(claimedCell)){
						scores.get(claimerId).addLateCell(claimedCell);
						claimed = true;
						}
					
					}
				}//for end
				//nobody claimed yet, adding to correct list
				if(!claimed){
					scores.get(claimerId).addCorrectCell(claimedCell);	
				}
			}
		
		}//end while iterator
	}
	
	private boolean correctMineral(Cell a, Cell b){
		return (a.getMineral().equalsIgnoreCase(b.getMineral()));
	}
	
	private void calculateFinalScore(ScoreCard sc){
		//calculates final score based on Scorecards state
		int totalCorrect = 0;
		for(Entry<Integer, ScoreCard> teamCard: scores.entrySet()){
			
				totalCorrect+=teamCard.getValue().getCorrectCells().size();
			
		}
		
		sc.setFinalScore((sc.getCorrectCells().size() - 5*sc.getWrongCells().size())*totalCorrect);
	}
	
	private void printScore(ScoreCard scoreCard){
		//prints scores
		System.out.println(scoreCard.toString());
	}
	
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = PlatformConstants.MISSION_LENGTH * 1000;
		// After mission length expires -> print score and terminate platform agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Mission ends
				System.out.println(myAgent.getLocalName()+":  ******************** \n Mission of " + PlatformConstants.MISSION_LENGTH + " seconds ends!\n" + " ******************** "); 
				
				// Print score
				for(Entry <Integer, ScoreCard> e : scores.entrySet()){
					calculateFinalScore(e.getValue());
					printScore(e.getValue());
					System.out.println("CORRECT CELLS:");
					for(Cell c : e.getValue().getCorrectCells()){
						System.out.println("(" + c.getX()+ ", " + c.getY() + ")->" + c.getMineral());
					}
					System.out.println("LATE CELLS:");
					for(Cell c : e.getValue().getLateCells()){
						System.out.println("(" + c.getX()+ ", " + c.getY() + ")->" + c.getMineral());
					}
					System.out.println("WRONG CELLS:");
					for(Cell c : e.getValue().getWrongCells()){
						System.out.println("(" + c.getX()+ ", " + c.getY() + ")->" + c.getMineral());
					}
				}
				// Terminate AgentSpacecraft
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
	
}
